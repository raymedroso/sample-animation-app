import React, { useState } from 'react';

import './App.css';
import BasicCSSTransitionWrapper from "./components/BasicCSSTransitionWrapper";
import data from "./data/data.json";

function App() {
  const [entry, setEntry] = useState(data.weatherEntry);

  return (
    <div className="App">
        {entry.map(({ComponentType, Children, CssProps}, key) => {
            if(ComponentType === 'BasicCSSTransitionWrapper')
                return <BasicCSSTransitionWrapper key={key} Children={Children} CssProps={CssProps}/>
            return null;
        })}
    </div>
  );
}

export default App;
