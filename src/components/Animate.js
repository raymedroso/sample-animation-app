import React from "react";
import { Transition } from 'react-transition-group';

import Text from "./Text";

const Animate = ({ isVisible, entry,  }) => {
    const { ComponentType, Value, CssProps, TransitionProps } = entry;
    const { DefaultStyles, TransitionStyles, duration } = TransitionProps;
    let child;

    if(ComponentType === "Text")
        child = <Text text={Value} cssProps={CssProps}/>
    // Other component types here
    else
        child = null;

    return <Transition in={isVisible} timeout={duration}>
        {state => (
            <div style={{
                ...DefaultStyles,
                ...TransitionStyles[state]
            }}>
                {child}
            </div>
        )}
    </Transition>
}

export default Animate;