import React, {useEffect, useState} from "react";
import Animate from "./Animate";

function BasicCSSTransitionWrapper({Children, CssProps = {}}) {
    const [current, setCurrent] = useState(0);

    useEffect(() => {
        // const interval = setInterval(() => {
        //     setCurrent(current => {
        //         if(current < Children.length - 1)
        //             return current + 1;
        //         else
        //             return 0;
        //     });
        // }, 3000);
        //
        setCurrent(current => current + 1)

        // return () => clearInterval(interval);
    }, [Children]);

    if(Children.length === 0)
        return null;

    return <div style={CssProps}>
        {Children.map((entry, key) => <Animate key={key} entry={entry} isVisible={current === key}/>)}
    </div>
}

export default BasicCSSTransitionWrapper;