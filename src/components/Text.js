import React from "react";

export default ({ text, cssProps = {}}) => {
    return <span style={cssProps}>{text}</span>
}